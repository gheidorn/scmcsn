# app.rb
require 'rubygems'
require 'bundler/setup'

# sinatra
require 'sinatra/base'
require 'sinatra/partial'

# view tech
require 'compass'
require 'haml'
require 'json'
require 'sass'

# database
require 'mongo'
require 'mongo_mapper'

# app-specific
require './app/model'
require './app/routes'

class App < Sinatra::Base
  set :haml, :format => :html5

  configure do
    begin
      MongoMapper.connection = Mongo::Connection.new('localhost')
      MongoMapper.database = 'scmcsn'
    rescue
      puts "could not connect to mongo"
    end
  end

  # bring in routes
  register Routes

  # partials support
  register Sinatra::Partial

end