require './app/model'
require 'haml'
require 'sass'
require 'compass'
require 'date'

module Routes

  def self.registered(app)

    app.get "/" do
      haml :index
    end

    app.get "/news" do
      haml :index
    end

    app.get "/news/:year" do
      haml :index
    end

    app.get "/news/:year/:week" do
      haml :index
    end

    app.get "/scores" do

      # grab the latest game
      begin
        latest_game = Game.sort(:week => -1, :year => -1).first()
      rescue
        p 'could not retrieve the latest game'
      end

      # grab the games for the latest year + week combination
      begin
        @games = Game.where(:year => latest_game[:year], :week => latest_game[:week]).sort(:field => 1)
      rescue
        p 'could not retrieve games for year 2013 and week 2 from the database'
      end

      haml :scoreboard, :locals => { :year => @games.first[:year], :week => @games.first[:week], :gameDate => @games.first[:gameDate], :numWeeks => latest_game[:week] }
    end

    app.get "/scores/:year" do
      begin
        @games = Game.where(:year => params[:year])
      rescue
        p 'could not retrieve games for year ' + params[:year] + ' from the database'
      end
      haml :scoreboard, :locals => { :year => @games.first[:year], :week => 1, :gameDate => @games.first[:gameDate] }
    end

    app.get "/scores/:year/week-:week" do
      begin
        @games = Game.where(:year => Integer(params[:year]), :week => Integer(params[:week])).sort(:field => 1)
      rescue
        p 'could not retrieve games for year '+params[:year]+' and week '+params[:week]+' from the database'
      end

      # grab the total number of weeks for this year so we can build week-to-week pagination
      begin
        game = Game.where(:year => Integer(params[:year])).sort(:week => -1).first
      rescue
        p 'could not retrieve numWeeks for year ' + params[:year]
      end

      haml :scoreboard, :locals => { :year => @games.first[:year], :week => @games.first[:week], :gameDate => @games.first[:gameDate], :numWeeks => game[:week] }
    end

    app.get "/standings" do
      year = Date.today.strftime("%Y")
      
      standings = Standing.where(:year => year.to_i).sort(:wins.desc, :runDiff.desc)
      
      # current year could be later than last season on record
      if standings.empty?
        year = year.to_i - 1
        standings = Standing.where(:year => year.to_i).sort(:wins.desc, :runDiff.desc)
      end

      haml :standings, :locals => { :request => request, :standings => standings, :year => year }
    end

    app.get "/standings/compile/:year" do
      # todo: cleanse year
      year = params[:year]

      # purge standings documents for the given year
      old_standings = Standing.all(:year => year.to_i)
      old_standings.each do |os|
        os.destroy if !os.nil?
        p 'deleted standings record for ' + os.year.to_s + ' ' + os.teamName
      end

      # gather all teams
      teams = Team.sort(:teamName).all()

      # gather home games for each team given the year
      teams.each do |t|
        standing = Standing.new(t.teamName, year)
        home_games = Game.where(:homeTeam => t.teamName, :year => Integer(params[:year])).sort(:week => -1)
        home_games.each do |hg|
          if hg.homeScore > hg.awayScore
            standing.wins += 1
          else
            standing.losses += 1
          end
          standing.runsFor += hg.homeScore
          standing.runsAgainst += hg.awayScore
        end

        away_games = Game.where(:awayTeam => t.teamName, :year => Integer(params[:year])).sort(:week => -1)

        # gather runs for
        away_games.each do |ag|
          if ag.awayScore > ag.homeScore
            standing.wins += 1
          else
            standing.losses += 1
          end
          standing.runsFor += ag.awayScore
          standing.runsAgainst += ag.homeScore
        end

        standing.winPercentage = standing.wins.fdiv( standing.wins + standing.losses )
        standing.runsForPG = standing.runsFor.fdiv( standing.wins + standing.losses )
        standing.runsAgainstPG = standing.runsAgainst.fdiv( standing.wins + standing.losses )
        standing.runDiff = standing.runsFor - standing.runsAgainst

        puts standing.teamName + ' => ' + standing.wins.to_s + ' - ' + standing.losses.to_s + '  (' + standing.winPercentage.to_s + ')  ' + standing.runsFor.to_s + ':' + standing.runsAgainst.to_s
        standing.save!
      end

      redirect to('/standings')
    end

    app.get "/standings/:year" do
      haml :standings
    end

    app.get "/standings/:year/:week" do
      haml :standings
    end

    app.get "/teams" do
      year = Date.today.strftime("%Y")
      @all_teams = Team.where(:year => Integer(year)).sort(:teamName).all()
      haml :team_list, :locals => { :year => year }
    end

    app.get "/teams/:year" do
      year = params[:year]
      @all_teams = Team.where(:year => Integer(year)).sort(:teamName).all()
      haml :team_list, :locals => { :year => year }
    end

    app.get "/teams/:year/:teamName" do
      @all_teams = Team.sort(:teamName).all()
      haml :team_list
    end

    app.get "/schedule" do
      haml :schedule
    end

    app.get "/schedule/:year" do
      p params[:year]
      haml :schedule
    end

    app.get "/recap/form" do
      haml :form_recap
    end

    app.post "/rest/recap" do
      recap = Recap.new(:author => params[:author],
                        :recap => params[:recap],
                        :game_date => params[:gameDate],
                        :home_team => params[:homeTeam],
                        :away_team => params[:awayTeam])
      puts "recap saved"
      puts recap
      status 200
    end

    app.get "/rest/team" do
      #@all_teams = Team.sort(:teamName).all()
      team = {}
      status 200
      @team.to_json
    end

    app.get "/rest/teams" do
      @all_teams = Team.sort(:teamName).all()
      status 200
      body(@all_teams.to_json)
    end

    app.put "/rest/team" do
      team = Team.new(:teamName => params[:teamName], :year => params[:year])
      team.save
      puts "**** put a new team @ " + team.id.to_s
      status 200
    end

  end

end