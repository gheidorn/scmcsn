#require 'rubygems'
#require 'sinatra'
#require 'json'
#require 'mongo'
require 'mongo_mapper'

class Team
  include MongoMapper::Document
  set_collection_name "team"

  key :teamName, String
  key :year, Integer

  many :players
end

class Recap
  include MongoMapper::Document
  set_collection_name "recap"

  key :author, String
  key :recap, String
  key :createdDate, Date
  key :gameDate, Date
  key :homeTeam, String
  key :awayTeam, String
end

class Player
  include MongoMapper::EmbeddedDocument
  key :firstName, String
  key :lastName, String
end

class Game
  include MongoMapper::Document
  set_collection_name "game"

  key :gameDate, String
  key :year, Integer
  key :week, Integer
  key :status, String
  key :inning, Integer
  key :field, String
  key :homeTeam, String
  key :homeScore, Integer
  key :awayTeam, String
  key :awayScore, Integer

end

class Standing
  include MongoMapper::Document
  set_collection_name "standing"

  key :teamName, String
  key :year, Integer
  key :wins, Integer
  key :losses, Integer
  key :winPercentage, Float
  key :runsFor, Integer
  key :runsAgainst, Integer
  key :runsForPG, Float
  key :runsAgainstPG, Float
  key :runDiff, Integer

  def initialize teamName, year
    @teamName = teamName
    @year = year
    @wins = 0
    @losses = 0
    @winPercentage = 0
    @runsFor = 0
    @runsAgainst = 0
    @runsForPG = 0
    @runsAgainstPG = 0
    @runDiff = 0
  end
end